#include "common/Application.hpp"
#include "common/CrossCapSurface.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>

class SampleApplication : public Application {
public:
    ShaderProgramPtr _shader;
    
    void makeScene() {
        Application::makeScene();
        _cameraMover = std::make_shared<OrbitCameraMover>();
        _shader = std::make_shared<ShaderProgram>(
            "./691YankinaData1/shaderNormal.vert",
            "./691YankinaData1/shader.frag"
            );
        
        _crossCapSurface = makeCrossCapSurface(1.0f, (int)(_detalizationFactor / 20));
        _crossCapSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
    }
    
    void draw() {
        Application::draw();

        //�������� ������� ������� ������ � ��������� �������
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        //������� ������ ����� � ������� �� ����������� ���������� ����������� �����
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //���������� ������
        _shader->use();
        
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        
        //��������� �� ���������� ������� ������ ����� � ��������� ���������
        _shader->setMat4Uniform("modelMatrix", _crossCapSurface->modelMatrix());
        _crossCapSurface->draw();
    }
	
private:
    MeshPtr _crossCapSurface;
    unsigned int _detalizationFactor = 100;
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}